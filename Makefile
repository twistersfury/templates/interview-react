######################################################
# Core Configurations (Do Not Edit)
######################################################
BIN_PATH ?= bin

MAKEFILE_JUSTNAME := $(firstword $(MAKEFILE_LIST))
MAKEFILE_COMPLETE := $(CURDIR)/$(MAKEFILE_JUSTNAME)
MAKE_OPTIONS := -f $(MAKEFILE_JUSTNAME)

######################################################
# Initial Values (Overrides Included Defaults)
######################################################

COMPOSER_IMAGE = twistersfury/phalcon:82-development
CODECEPT_IMAGE = twistersfury/phalcon:82-development

######################################################
# Default Command (Must Be First)
######################################################
.PHONY: default
default: test

######################################################
# Include Docker
######################################################
ifndef MAKEFILE_DOCKER

include $(BIN_PATH)/docker.mk

$(BIN_PATH)/docker.mk:
	$(MAKEFILE_DOWNLOAD_COMMAND) https://gitlab.com/twistersfury/utilities/raw/v2.0.0/make/docker.mk
endif

######################################################
# Include Node
######################################################
ifndef MAKEFILE_NODE

include $(BIN_PATH)/node.mk

$(BIN_PATH)/node.mk: | $(BIN_PATH)
	$(MAKEFILE_DOWNLOAD_COMMAND) https://gitlab.com/twistersfury/utilities/raw/v2.1.3/make/node.mk
endif

######################################################
# Include Local Overrides
######################################################
# If you don't have wget, then you can manually edit
# 	bin/overrides.mk to enable curl

$(BIN_PATH):
	mkdir -p $@

$(BIN_PATH)/overrides.mk: | $(BIN_PATH)
	echo 'MAKEFILE_OVERRIDES := true' > $@
	echo 'MAKEFILE_DOWNLOAD_COMMAND = wget -O $$@' >> $@
	echo '#MAKEFILE_DOWNLOAD_COMMAND = curl -o $$@' >> $@
	echo "Override File Created, You Will Have To Rerun The Previous Command."
	echo "By default, this script attempts to use wget. If you don't have wget, you can use curl instead."
	echo "If you want to use curl, edit the $@ file to use the curl version of MAKEFILE_DOWNLOAD_COMMAND." && exit 1

# Only Include If Not Already Included (Will Cause Errors Otherwise)
ifndef MAKEFILE_OVERRIDES

include $(BIN_PATH)/overrides.mk

endif

######################################################
# Core Commands
######################################################

.PHONY: clean
clean:
	rm -rf vendor
	rm -rf bin